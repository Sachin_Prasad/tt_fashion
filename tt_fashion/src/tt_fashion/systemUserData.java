/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tt_fashion;

/**
 *
 * @author Kushan
 */
public class systemUserData {

    private static String user = "Teshan";
    private static String userType = "AD1";
    private static String systemUser = "AD1";

    public static String getCurrentUser() {
        return user;
    }

    public static void setCurrentUser(String aCurrentUser) {
        user = aCurrentUser;
    }

    public static String getUserType() {
        return userType;
    }

    public static void setUserType(String ut) {
        userType = ut;
    }

    public static void setUserID(String emp) {
        systemUser = emp;
    }

    public static String getUserID() {
        return systemUser;
    }
}